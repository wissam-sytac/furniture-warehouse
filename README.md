# Furniture Warehouse

This document explains the deliverable, and outlines the assumptions I made while working on this assignment.

While this is far from being production ready (due to time constraints), I hope it gives you an idea about my problem-solving skills etc.

💡 To run this app, refer to the `INSTRUCTIONS.md` file.

## Deliverable breakdown
* The entry point of this app is `index.ts` located under `src/app/`.
* The requirements are outlined in `index.ts`.
* The app sources the contents of the files in `inputs/`, normalizes the data and stores it in memory
* The file contents are validated before being ingested  
* The app will exit if it's unable to source the article inventory and product catalogs.
* The app displays the estimated maximum inventory for each product in the catalog based on the inventory on hand
* Takes sample orders and determines whether orders can be fulfilled
* Orders are being taken from `inputs/products.json`. As mentioned in the todos, in future iterations we may take orders from CLI or other fashion.
* Unit tests: covers mostly the domain layer. see: `/src/domain` 

CLI             |  Tests
:-------------------------:|:-------------------------:
![screenshot](./docs/imgs/tests.png)  |  ![screenshot](./docs/imgs/cli-screenshot.png)

## Technology Choices

### Why NodeJS/Typescript?
* NodeJS is quick to setup and easy to work with 
* Your stack is based on TS/NodeJS (based on the info that was shared with me)
* I am personally comfortable with Node
* Plays well with both OO and FP styles
* I don't anticipate any heavy computations that may block the event loop and possibly slow down the application as a result

### Why no Framework?
* Wanted to freely mix approaches without being tied to a specific framework
* Intended to showcase skills without hiding behind a framework
* We can easily add a framework to complement the existing codebase, e.g. to handle things like HTTP Requests, Event sourcing, etc.

### Why Functional Programming?
* We are using Functional Programming style because I was informed that that's what you are using
* We started building from the core domain and out. A lot of the business domain concepts and operations in this case can be modelled quite easily using a functional paradigm.
* limitations: As we begin to edge towards the outer layers of the applications, for example as we introduce Database persistence etc. it may turn out to be a bit challenging to stick to 100% FP

## Assumptions
- Used ProductName as unique identifier for products
- Product inventory is an estimated number.
- Product in this domain is just a collection of articles. Price is irrelevant in this domain based on my understanding.
- We assume that orders contain legitimate product names. All products names that cannot be found in the product catalog are simply ignored (no warning etc.).  
- For simplicity, the inventory can only be loaded initially (at application start up) and not at any time during the application.
- For simplicity, and due to time constraints, application state is maintained in memory. In other words, when the application exists, state is lost.
- Performing transactions is not ideal at the moment. The inventory is just updated manually and on the fly. there is no record of what happened.
- Regarding testing, we focused our effort on implementing the domain layer in a TDD fashion in order to ensure a robust core of business logic as we grow the app outwards.
We understand that the domain logic is the most crucial and we wanted to make sure that this part remains functional and bug free as we refactor and work on more layers.
- If time allows we will write additional tests to cover the app layer.  

## How can I run this project?
💡 Refer to `INSTRUCTIONS.md` file.

## Architecture
- Domain: contains the core business logic of this business domain. Lives in `src/domain`
- Application logic: lives in `src/app`.

## Future iterations:
- [ ] Add proper persistence layer to replace in-memory state management
- [ ] Add Event Sourcing to transactions. Currently, we just update the inventory manually.
- [ ] Expand testing beyond the domain layer of the application
- [ ] Take orders from CLI
