# Instructions

* Pre-requisites: Node installed version >= v14.x


* Install dependencies:
```
npm install
```

* Run tests:
```
npm t
```

* Run locally:
```
npm run dev
```

* Build application:
```
npm run build
```

* Run application:
```
npm run prod
```
