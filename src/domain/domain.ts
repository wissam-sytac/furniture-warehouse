export type ArticleId = string;

export type Quantity = number;

export type ArticleName = string;

export type ProductName = string;

export type ArticleArrangement = Record<ArticleId, Quantity>;

export type SumArticleArrangements = (a1: ArticleArrangement, a2: ArticleArrangement) => ArticleArrangement;

export type MultiplyArticleArrangements = (a: ArticleArrangement, factor: number) => ArticleArrangement;

export type SubtractArticleArrangements = (a1: ArticleArrangement, a2: ArticleArrangement) => ArticleArrangement;

export type Article = {
  id: ArticleId;
  name: ArticleName;
};

export type Product = {
  name: ProductName;
  arrangement: ArticleArrangement;
};

export type OrderRow = {
  product: Product;
  quantity: Quantity;
};

export type Order = OrderRow[];

export type ArticleInventory = ArticleArrangement;

export type ArticleCatalog = Record<ArticleId, ArticleName>;

export type ProductCatalog = Record<ProductName, ArticleArrangement>;

export type ProductInventory = Record<ProductName, Quantity>;
