import { ArticleInventory, Order } from '../domain';
import { calculateMaximumQuantity } from '../article-arrangements/calculate-maximum-quantity';
import { calculateOrderFulfillment } from './calculate-order-fulfillment';

export function canOrderBeFulfilled(order: Order, articleInventory: ArticleInventory) {
  const orderFulfillment = calculateOrderFulfillment(order);
  return calculateMaximumQuantity(orderFulfillment, articleInventory) > 0;
}
