import { ArticleArrangement, Order } from '../domain';
import { sumArticleArrangements } from '../article-arrangements/sum';
import { multiplyArticleArrangements } from '../article-arrangements/multiply';

export function calculateOrderFulfillment(order: Order): ArticleArrangement {
  return order.reduce((fulfillment: ArticleArrangement, { product, quantity }) => {
    return sumArticleArrangements(fulfillment, multiplyArticleArrangements(product.arrangement, quantity));
  }, {});
}
