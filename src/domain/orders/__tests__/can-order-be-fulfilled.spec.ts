import { canOrderBeFulfilled } from '../can-order-be-fulfilled';
import { ArticleInventory } from '../../domain';

const inventory: ArticleInventory = {
  '1': 20,
  '2': 20,
  '3': 20,
  '4': 20,
  '5': 20,
  '6': 20,
  '7': 20,
  '8': 20,
};

describe('canOrderBeFulfilled', () => {
  it('should work correctly', () => {
    expect(
      canOrderBeFulfilled(
        [
          {
            product: {
              name: 'p1',
              arrangement: { '1': 20, '2': 20 },
            },
            quantity: 1,
          },
        ],
        inventory,
      ),
    ).toEqual(true);

    expect(
      canOrderBeFulfilled(
        [
          {
            product: {
              name: 'p1',
              arrangement: { '1': 20, '2': 20 },
            },
            quantity: 2,
          },
        ],
        inventory,
      ),
    ).toEqual(false);
  });
});
