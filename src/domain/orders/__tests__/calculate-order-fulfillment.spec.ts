import { calculateOrderFulfillment } from '../calculate-order-fulfillment';

describe('calculateOrderFulfillment', () => {
  it('should work correctly', () => {
    expect(calculateOrderFulfillment([])).toEqual({});

    expect(
      calculateOrderFulfillment([
        {
          product: {
            name: 'p1',
            arrangement: { '1': 2, '2': 4 },
          },
          quantity: 0,
        },
      ]),
    ).toEqual({ '1': 0, '2': 0 });

    expect(
      calculateOrderFulfillment([
        {
          product: {
            name: 'p1',
            arrangement: { '1': 2, '2': 4 },
          },
          quantity: 1,
        },
      ]),
    ).toEqual({ '1': 2, '2': 4 });

    expect(
      calculateOrderFulfillment([
        {
          product: {
            name: 'p1',
            arrangement: { '1': 2, '2': 4 },
          },
          quantity: 2,
        },
        {
          product: {
            name: 'p2',
            arrangement: { '1': 2, '3': 10 },
          },
          quantity: 3,
        },
      ]),
    ).toEqual({ '1': 10, '2': 8, '3': 30 });
  });
});
