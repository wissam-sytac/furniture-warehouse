import { ArticleArrangement, ArticleId, ArticleInventory, Quantity } from '../domain';
import { isEmptyArrangement } from './is-empty';

function minQuantity(input: Quantity[]): number {
  if (!input.length) {
    return 0;
  }
  return input.reduce((acc, qty) => {
    return qty < acc ? qty : acc;
  }, input[0]);
}

export function calculateMaximumQuantity(arrangement: ArticleArrangement, inventory: ArticleInventory): number {
  // convention
  if (isEmptyArrangement(arrangement)) {
    return 1;
  }

  if (isEmptyArrangement(inventory)) {
    return 0;
  }

  const distribution = Object.keys(arrangement).reduce((acc: ArticleArrangement, articleId: ArticleId) => {
    if (!inventory[articleId]) {
      acc[articleId] = 0;
    } else {
      acc[articleId] = Math.floor(inventory[articleId] / arrangement[articleId]);
    }
    return acc;
  }, {});

  return minQuantity(Object.values(distribution));
}
