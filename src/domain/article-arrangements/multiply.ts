import { ArticleArrangement, MultiplyArticleArrangements } from '../domain';

export const multiplyArticleArrangements: MultiplyArticleArrangements = function (arrangement, factor) {
  const factorAsInt = Math.floor(factor);
  return Object.keys(arrangement).reduce((acc: ArticleArrangement, articleId) => {
    acc[articleId] = arrangement[articleId] * factorAsInt;
    return acc;
  }, {});
};
