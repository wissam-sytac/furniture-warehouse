import { ArticleArrangement, SumArticleArrangements } from '../domain';

export const sumArticleArrangements: SumArticleArrangements = function (a1, a2) {
  // Copy contents of a1, as if we're cloning
  const result = Object.keys(a1).reduce((acc: ArticleArrangement, articleId) => {
    acc[articleId] = a1[articleId];
    return acc;
  }, {});

  return Object.keys(a2).reduce((acc, articleId) => {
    acc[articleId] = a2[articleId] + (acc[articleId] || 0);
    return acc;
  }, result);
};
