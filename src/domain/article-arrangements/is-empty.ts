import { ArticleArrangement } from '../domain';

export function isEmptyArrangement(arrangement: ArticleArrangement) {
  return !Object.keys(arrangement).length;
}
