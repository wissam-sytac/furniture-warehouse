import { ArticleArrangement, SubtractArticleArrangements } from '../domain';
import { calculateMaximumQuantity } from './calculate-maximum-quantity';
import { isEmptyArrangement } from './is-empty';

export const subtractArticleArrangements: SubtractArticleArrangements = function (a1, a2) {
  const maximumAvailability = calculateMaximumQuantity(a2, a1);
  if (maximumAvailability === 0) {
    return {};
  }
  if (isEmptyArrangement(a2)) {
    return a1;
  }
  return Object.keys(a1).reduce((acc: ArticleArrangement, articleId) => {
    acc[articleId] = a1[articleId] - (a2[articleId] || 0);
    return acc;
  }, {});
};
