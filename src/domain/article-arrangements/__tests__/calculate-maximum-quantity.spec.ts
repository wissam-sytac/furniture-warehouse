import { calculateMaximumQuantity } from '../calculate-maximum-quantity';
import { ArticleInventory } from '../../domain';

const inventory: ArticleInventory = {
  '1': 20,
  '2': 20,
  '3': 20,
  '4': 20,
  '5': 20,
  '6': 20,
  '7': 20,
  '8': 20,
};

describe('computeMaximumProductInventory', () => {
  it('should handle case of empty sets', () => {
    expect(calculateMaximumQuantity({}, {})).toEqual(1); // convention
    expect(calculateMaximumQuantity({}, inventory)).toEqual(1); // convention
    expect(calculateMaximumQuantity({ '1': 1 }, {})).toEqual(0);
  });

  it('should work correctly', () => {
    expect(calculateMaximumQuantity({ '1': 2 }, inventory)).toEqual(10);
    expect(calculateMaximumQuantity({ '1': 2, '2': 21 }, inventory)).toEqual(0);
    expect(calculateMaximumQuantity({ '9': 2 }, inventory)).toEqual(0);
    expect(calculateMaximumQuantity({ '5': 3, '6': 6, '8': 2 }, inventory)).toEqual(3);
  });
});
