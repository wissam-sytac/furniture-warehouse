import { sumArticleArrangements } from '../sum';

describe('sumArticleArrangements', () => {
  it('should handle empty set correctly', () => {
    expect(sumArticleArrangements({}, {})).toEqual({});
    expect(sumArticleArrangements({}, { '1': 10 })).toEqual({ '1': 10 });
    expect(sumArticleArrangements({ '1': 10 }, {})).toEqual({ '1': 10 });
  });

  it('should work in regular cases', () => {
    expect(sumArticleArrangements({ '1': 10 }, { '2': 5 })).toEqual({ '1': 10, '2': 5 });
    expect(sumArticleArrangements({ '1': 10 }, { '1': 4, '2': 5 })).toEqual({ '1': 14, '2': 5 });
    expect(sumArticleArrangements({ '1': 10, '2': 7 }, { '1': 4 })).toEqual({ '1': 14, '2': 7 });
  });
});
