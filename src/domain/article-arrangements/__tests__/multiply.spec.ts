import { multiplyArticleArrangements } from '../multiply';

describe('multiplyArticleArrangements', () => {
  it('should handle zero and identity and empty set', () => {
    expect(multiplyArticleArrangements({}, 0)).toEqual({});
    expect(multiplyArticleArrangements({}, 1)).toEqual({});
    expect(multiplyArticleArrangements({ '1': 1 }, 0)).toEqual({ '1': 0 });
    expect(multiplyArticleArrangements({ '1': 10, '2': 4 }, 0)).toEqual({ '1': 0, '2': 0 });
  });

  it('should handle strictly positive integer factors', () => {
    expect(multiplyArticleArrangements({ '1': 1 }, 1)).toEqual({ '1': 1 });
    expect(multiplyArticleArrangements({ '1': 1 }, 5)).toEqual({ '1': 5 });
    expect(multiplyArticleArrangements({ '1': 10, '2': 4 }, 3)).toEqual({ '1': 30, '2': 12 });
    expect(multiplyArticleArrangements({ '1': 0, '2': 4 }, 2)).toEqual({ '1': 0, '2': 8 });
  });
});
