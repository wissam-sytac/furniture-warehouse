import { isEmptyArrangement } from '../is-empty';

describe('isEmptyArrangement', () => {
  it('should work correctly', () => {
    expect(isEmptyArrangement({})).toEqual(true);
    expect(isEmptyArrangement({ '0': 0 })).toEqual(false);
    expect(isEmptyArrangement({ '1': 0, '2': 1 })).toEqual(false);
  });
});
