import { subtractArticleArrangements } from '../subtract';

describe('subtractArticleArrangements', () => {
  it('should handle case of a2 is NOT part of a1', () => {
    expect(subtractArticleArrangements({}, {})).toEqual({});
    expect(subtractArticleArrangements({ '1': 3 }, { '1': 7 })).toEqual({});
    expect(subtractArticleArrangements({ '1': 3 }, { '2': 1 })).toEqual({});
    expect(subtractArticleArrangements({ '1': 3 }, { '1': 1, '2': 1 })).toEqual({});
  });

  it('should handle case of a2 is part of a1', () => {
    expect(subtractArticleArrangements({ '1': 10, '2': 10 }, {})).toEqual({ '1': 10, '2': 10 });
    expect(subtractArticleArrangements({ '1': 10, '2': 10 }, { '1': 2 })).toEqual({ '1': 8, '2': 10 });
    expect(subtractArticleArrangements({ '1': 10, '2': 10 }, { '1': 3, '2': 4 })).toEqual({ '1': 7, '2': 6 });
  });
});
