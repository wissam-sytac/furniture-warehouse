import { readFile } from 'fs';

export async function readFileAsync(filePath: string): Promise<string> {
  return new Promise((resolve, reject) => {
    readFile(filePath, (err, data) => {
      if (err) {
        return reject(err);
      }
      if (!data) {
        return reject(new Error('Invalid file contents'));
      }
      return resolve(data.toString());
    });
  });
}

export async function readFileAsJson(filePath: string) {
  const fileContents = await readFileAsync(filePath);
  return JSON.parse(fileContents);
}
