import { Product, ProductCatalog, ProductName } from '../../domain/domain';

export function findProductByName(productCatalog: ProductCatalog, productName: ProductName): Product | undefined {
  const match = productCatalog[productName];
  if (!match) {
    return undefined;
  }
  return {
    name: productName,
    arrangement: match,
  };
}
