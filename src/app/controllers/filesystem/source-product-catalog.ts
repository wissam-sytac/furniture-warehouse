import { readFileAsJson } from '../../../utils/readFileAsJSON';
import { ArticleArrangement, ProductCatalog } from '../../../domain/domain';
import { ProductInputDto } from '../../dtos/product-input-dto';
import { validateAndTransformProductInputs } from '../../validators/validate-transform-product-inputs';

export async function sourceProductCatalog(filePath: string): Promise<ProductCatalog> {
  const fileInput = await readFileAsJson(filePath);
  let validatedProducts: ProductInputDto[] = [];

  try {
    validatedProducts = await validateAndTransformProductInputs(fileInput);
  } catch (err) {
    throw err;
  }

  return validatedProducts.reduce((acc: ProductCatalog, item) => {
    acc[item.name] = item.contain_articles.reduce((mappings: ArticleArrangement, i) => {
      mappings[i.art_id] = i.amount_of;
      return mappings;
    }, {});
    return acc;
  }, {});
}
