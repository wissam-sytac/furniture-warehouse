import { readFileAsJson } from '../../../utils/readFileAsJSON';
import { ArticleCatalog, ArticleInventory } from '../../../domain/domain';
import { ArticleInput } from '../../dtos/article-input-dto';
import { validateAndTransformArticleInputs } from '../../validators/validate-transform-article-inputs';

export type ArticleDB = {
  catalog: ArticleCatalog;
  inventory: ArticleInventory;
};

export async function sourceArticlesFromFile(filePath: string): Promise<ArticleDB> {
  const fileInput = await readFileAsJson(filePath);
  let validatedArticles: ArticleInput[] = [];

  try {
    validatedArticles = await validateAndTransformArticleInputs(fileInput);
  } catch (err) {
    throw err;
  }

  const articleInventory = validatedArticles.reduce((acc: ArticleInventory, { art_id, stock }) => {
    acc[art_id] = stock + (acc[art_id] || 0);
    return acc;
  }, {});

  const articleCatalog = validatedArticles.reduce((acc: ArticleCatalog, { art_id, name }) => {
    acc[art_id] = name;
    return acc;
  }, {});

  return {
    catalog: articleCatalog,
    inventory: articleInventory,
  };
}
