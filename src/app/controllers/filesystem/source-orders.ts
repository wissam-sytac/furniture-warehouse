import { readFileAsJson } from '../../../utils/readFileAsJSON';
import { validateTransformInputOrder } from '../../validators/validate-transform-input-order';
import { InputOrderDto, InputOrderRowDto } from '../../dtos/input-order-dto';

export async function sourceOrdersFromFile(filePath: string): Promise<InputOrderDto[]> {
  const fileInput = await readFileAsJson(filePath);
  let inputOrders: InputOrderRowDto[][] = [];

  try {
    inputOrders = await validateTransformInputOrder(fileInput);
    return inputOrders;
  } catch (err) {
    throw err;
  }
}
