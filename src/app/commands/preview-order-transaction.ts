import { ArticleInventory, ProductCatalog } from '../../domain/domain';
import { InputOrderDto } from '../dtos/input-order-dto';
import { mapInputOrderToOrder } from './mappers';
import { calculateOrderFulfillment } from '../../domain/orders/calculate-order-fulfillment';
import { subtractArticleArrangements } from '../../domain/article-arrangements/subtract';

export function previewOrderTransaction(
  inventory: ArticleInventory,
  productCatalog: ProductCatalog,
  inputOrder: InputOrderDto,
) {
  const order = mapInputOrderToOrder(inputOrder, productCatalog);
  const orderFulfillment = calculateOrderFulfillment(order);
  return subtractArticleArrangements(inventory, orderFulfillment);
}
