import { calculateProductsInventory } from '../calculate-products-inventory';
import { ArticleInventory, ProductCatalog } from '../../../domain/domain';
import * as calculateMaximumQtyModule from '../../../domain/article-arrangements/calculate-maximum-quantity';

describe('calculateProductsInventory', () => {
  it('should handle empty inventory', () => {
    const productCatalog: ProductCatalog = {
      p1: {
        '1': 2,
        '3': 1,
      },
      p2: {
        '1': 4,
        '2': 4,
      },
    };
    expect(calculateProductsInventory({}, {})).toEqual({});
    expect(calculateProductsInventory(productCatalog, {})).toEqual({
      p1: 0,
      p2: 0,
    });
  });

  it('should calculate maximum quantity for each product', () => {
    const productCatalog: ProductCatalog = {
      p1: {
        '1': 2,
        '3': 1,
      },
      p2: {
        '1': 4,
        '2': 4,
      },
    };
    const articleInventory: ArticleInventory = {
      '1': 10,
      '2': 10,
      '3': 5,
    };

    const funcSpy = jest.spyOn(calculateMaximumQtyModule, 'calculateMaximumQuantity');

    calculateProductsInventory(productCatalog, articleInventory);

    expect(funcSpy).toHaveBeenCalledTimes(2);
    expect(funcSpy).toHaveBeenCalledWith(productCatalog.p1, articleInventory);
    expect(funcSpy).toHaveBeenCalledWith(productCatalog.p2, articleInventory);
  });
});
