import { InputOrderDto } from '../dtos/input-order-dto';
import { Order, ProductCatalog } from '../../domain/domain';
import { findProductByName } from '../repositories/products';

export function mapInputOrderToOrder(inputOrder: InputOrderDto, productCatalog: ProductCatalog): Order {
  return inputOrder.reduce((acc: Order, { productName, quantity }) => {
    const product = findProductByName(productCatalog, productName);
    if (!product) {
      return acc.concat([]);
    }
    return acc.concat({
      product,
      quantity,
    });
  }, []);
}
