import { ArticleInventory, ProductCatalog } from '../../domain/domain';
import { canOrderBeFulfilled } from '../../domain/orders/can-order-be-fulfilled';
import { InputOrderDto } from '../dtos/input-order-dto';
import { mapInputOrderToOrder } from './mappers';

export function determineOrderFulfillment(
  inventory: ArticleInventory,
  productCatalog: ProductCatalog,
  productOrder: InputOrderDto,
): boolean {
  const order = mapInputOrderToOrder(productOrder, productCatalog);
  return canOrderBeFulfilled(order, inventory);
}
