import { ArticleInventory, ProductInventory, ProductCatalog } from '../../domain/domain';
import { calculateMaximumQuantity } from '../../domain/article-arrangements/calculate-maximum-quantity';

export function calculateProductsInventory(
  productCatalog: ProductCatalog,
  inventory: ArticleInventory,
): ProductInventory {
  return Object.keys(productCatalog).reduce((acc: ProductInventory, productName: string) => {
    acc[productName] = calculateMaximumQuantity(productCatalog[productName], inventory);
    return acc;
  }, {});
}
