const { AsciiTable3, AlignmentEnum } = require('ascii-table3');

export function printInventory(item: string, inventory: Record<string, number>): void {
  const arr = Object.keys(inventory).reduce((acc: any[], key) => {
    return acc.concat([[key, inventory[key]]]);
  }, []);

  const table = new AsciiTable3('Inventory')
    .setHeading(item, 'Qty')
    .setAlign(2, AlignmentEnum.CENTER)
    .addRowMatrix(arr);

  console.log(table.toString());
}
