import { InputOrderRowDto } from '../dtos/input-order-dto';

const { AsciiTable3, AlignmentEnum } = require('ascii-table3');

export function printOrder(inputOrder: InputOrderRowDto[], orderLabel: string | number = ''): void {
  const arr = inputOrder.reduce((acc: any[], inputOrderRow) => {
    return acc.concat([[inputOrderRow.productName, inputOrderRow.quantity]]);
  }, []);

  const table = new AsciiTable3('Order: ' + orderLabel)
    .setHeading('Product', 'Qty')
    .setAlign(2, AlignmentEnum.CENTER)
    .addRowMatrix(arr);

  console.log(`\n${table.toString()}`);
}
