import 'reflect-metadata';
import path from 'path';
import { sourceArticlesFromFile } from './controllers/filesystem/source-articles';
import { sourceProductCatalog } from './controllers/filesystem/source-product-catalog';
import { ProductInventory } from '../domain/domain';
import { calculateProductsInventory } from './commands/calculate-products-inventory';
import { determineOrderFulfillment } from './commands/determine-order-can-be-fulfilled';
import { previewOrderTransaction } from './commands/preview-order-transaction';
import { printInventory } from './presenters/print-inventory';
import { printOrder } from './presenters/print-order';
import { sourceOrdersFromFile } from './controllers/filesystem/source-orders';
import { InputOrderDto } from './dtos/input-order-dto';

(async () => {
  // Inputs
  const inputsFolder = path.join(__dirname, '/../../inputs/');
  const inputFileArticles = path.join(inputsFolder, 'articles.json');
  const inputFileProducts = path.join(inputsFolder, 'products.json');
  const inputFileOrders = path.join(inputsFolder, 'orders.json');

  let articleDB,
    productCatalog,
    inputOrders: InputOrderDto[] = [];

  // Source data into memory
  // Exit application if unable to source
  try {
    [articleDB, productCatalog] = await Promise.all([
      sourceArticlesFromFile(inputFileArticles),
      sourceProductCatalog(inputFileProducts),
    ]);
  } catch (err) {
    console.error(`Unable to source product catalog article and/or articles: ${JSON.stringify(err)}`);
    process.exit(0);
  }

  // Compute max product inventory on hand
  const maxProductInventoryOnHand: ProductInventory = calculateProductsInventory(productCatalog, articleDB.inventory);
  console.info('[1] The Maximum Product Inventory On Hand: ');
  printInventory('Product', maxProductInventoryOnHand);

  console.info('===> Loading orders from input: ');
  try {
    inputOrders = await sourceOrdersFromFile(inputFileOrders);
    for (let i = 0; i < inputOrders.length; ++i) {
      printOrder(inputOrders[i], i);
    }
  } catch (err) {
    console.error(`Unable to source orders: ${JSON.stringify(err)}`);
  }

  /////////////////////////////////////////////////////////////
  // Assume orders loaded correctly from file.

  if (inputOrders.length < 3) {
    console.error('we assume that at least 3 orders are loaded to continue execution.');
    process.exit(1);
  }

  // Determine if order can be fulfilled
  // Orders are taken from the "fixtures/input-orders" file
  console.info('[2] Verify if an order can be fulfilled: ');

  // Fulfillable order 1
  printOrder(inputOrders[0], '1');
  const fulfillmentCheck = determineOrderFulfillment(articleDB.inventory, productCatalog, inputOrders[0]);
  console.log(`Can order be fulfilled? ${fulfillmentCheck}`);

  // Fulfillable order 1
  printOrder(inputOrders[2], '2');
  const fulfillmentCheck2 = determineOrderFulfillment(articleDB.inventory, productCatalog, inputOrders[2]);
  console.log(`Can order be fulfilled? ${fulfillmentCheck2}`);

  // Preview inventory if order transaction takes place
  console.info('\n\n[3] Preview ending inventory when transaction is complete: ');
  const inventoryAfterTransaction = previewOrderTransaction(articleDB.inventory, productCatalog, inputOrders[0]);
  printInventory('Article', inventoryAfterTransaction);

  // Manually update the inventory
  // No event sourcing or anything of the sort. Simple update of the inventory in memory.
  console.info('\n\n[4] Commit transaction and update inventory: ');
  articleDB.inventory = inventoryAfterTransaction;
  printInventory('Article', articleDB.inventory);
})();
