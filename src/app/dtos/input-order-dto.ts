import { IsArray, IsNotEmpty, IsNumber, IsString, Min, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class InputOrderRowDto {
  @IsString()
  @IsNotEmpty()
  productName: string;

  @IsNumber()
  @IsNotEmpty()
  @Min(0)
  quantity: number;
}

export type InputOrderDto = InputOrderRowDto[];

export class InputOrdersDto {
  @Type(() => InputOrderRowDto)
  @ValidateNested({ each: true })
  @IsArray()
  orders: InputOrderRowDto[][];
}
