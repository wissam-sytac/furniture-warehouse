import { IsArray, IsNotEmpty, IsNumber, IsString, Min, ValidateNested } from 'class-validator';
import { Transform, Type } from 'class-transformer';

export class ProductArticleInput {
  @IsString()
  @IsNotEmpty()
  art_id: string;

  @IsNumber()
  @IsNotEmpty()
  @Transform(({ value }) => Number(value))
  @Min(0)
  amount_of: number;
}

export class ProductInputDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsNumber()
  @Min(0)
  price: number;

  @Type(() => ProductArticleInput)
  @ValidateNested({ each: true })
  @IsArray()
  contain_articles: ProductArticleInput[];
}

export class Products {
  @Type(() => ProductInputDto)
  @ValidateNested({ each: true })
  @IsArray()
  products: ProductInputDto[];
}
