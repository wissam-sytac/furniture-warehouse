import { IsArray, IsNotEmpty, IsNumber, IsString, Min, ValidateNested } from 'class-validator';
import { Transform, Type } from 'class-transformer';

export class ArticleInput {
  @IsString()
  @IsNotEmpty()
  art_id: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsNumber()
  @IsNotEmpty()
  @Transform(({ value }) => Number(value))
  @Min(0)
  stock: number;
}

export class Articles {
  @Type(() => ArticleInput)
  @ValidateNested({ each: true })
  @IsArray()
  articles: ArticleInput[];
}
