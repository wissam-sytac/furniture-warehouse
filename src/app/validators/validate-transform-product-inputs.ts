import { ProductInputDto, Products } from '../dtos/product-input-dto';
import { transformAndValidate } from 'class-transformer-validator';

export async function validateAndTransformProductInputs(input: any): Promise<ProductInputDto[]> {
  try {
    const result = (await transformAndValidate(Products, input)) as Products;
    return result.products;
  } catch (err) {
    throw new Error(`Validation Error: ${JSON.stringify(err)}`);
  }
}
