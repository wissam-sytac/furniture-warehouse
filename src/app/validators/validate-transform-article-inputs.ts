import { ArticleInput, Articles } from '../dtos/article-input-dto';
import { transformAndValidate } from 'class-transformer-validator';

export async function validateAndTransformArticleInputs(input: any): Promise<ArticleInput[]> {
  try {
    const result = (await transformAndValidate(Articles, input)) as Articles;
    return result.articles;
  } catch (err) {
    throw new Error(`Validation Error: ${JSON.stringify(err)}`);
  }
}
