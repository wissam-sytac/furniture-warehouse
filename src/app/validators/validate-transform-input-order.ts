import { transformAndValidate } from 'class-transformer-validator';
import { InputOrderRowDto, InputOrdersDto } from '../dtos/input-order-dto';

export async function validateTransformInputOrder(input: any): Promise<InputOrderRowDto[][]> {
  try {
    const result = (await transformAndValidate(InputOrdersDto, input)) as InputOrdersDto;
    return result.orders;
  } catch (err) {
    throw new Error(`Validation Error: ${JSON.stringify(err)}`);
  }
}
